## Setting Up the Development Environment

Before you start contributing to the project, you need to set up your development environment. Follow these steps:

1. Clone the repository to your local machine:

```bash
git clone https://gitlab.com/deClot/template-matching.git
cd template-matching
```

2. Install Python (version 3.11.\*) if you haven't already.
3. Install pdm as a package manager
   `pip install pdm`
4. Install project dependencies:
   `pdm install`

## Setting Up Linters

Our project uses linting tools to maintain code quality. To set up linters locally, follow these steps:

1. Ensure you have pre-commit installed:
   `pdm list | grep pre-commit`
2. If pre-commit is not installed, install it:
   `pdm add pre-commit`
3. Set up pre-commit hooks by running:

```
pre-commit install
pre-commit install --hook-type commit-msg
```

## Running Linters

Linting checks are automatically executed when you run the git commit command or push changes to the repository. The pre-commit hook is configured to run all linters on staged files before allowing the commit to proceed. If any linting errors are found, the commit will be blocked until the issues are resolved. To manually run linting checks outside of the commit process, execute the following command in the project root directory:

```
pre-commit run --all-files
```
