- [Template Matching](#template-matching)
  - [Repository Management Methodology - GitHub Flow](#repository-management-methodology---github-flow)
- [Assignments](#assignments)
  - [Assignment - Docker](#assignment---docker)

# Template Matching

## Repository Management Methodology - GitHub Flow

Key Steps:

1. Create a branch (git checkout -b \<branch_name>) to work on a new feature or fix.
2. Add and commit your changes, and then push the branch to the remote repository (git push origin \<branch_name>).
3. Create a pull request on GitHub to discuss and review the changes.

# Assignments

## Assignment - Docker

Будет выполнено в ветке [docker-setup](https://gitlab.com/deClot/template-matching/-/tree/docker-setup?ref_type=heads#assignment-docker)

Keypoints:

- используем готовый образ с установленными зависимотсями для opencv (`pachyderm/opencv:2.6.0`)
- используем `pdm` в качестве менеджера пакетов
- для сборки образа
  `docker build . -t template_matching`
- для запуска контейнера
  `docker run -it --entrypoint /bin/bash -p 8888:8888 --rm template_matching`
- для запуска jupyter через консоль
  `jupyter notebook --port=8888 --ip=0.0.0.0 --alow-root`
