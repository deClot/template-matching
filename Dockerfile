
ARG OPENCV=pachyderm/opencv:2.6.0
FROM $OPENCV

RUN apt-get update && apt-get install -y libgl1-mesa-glx

RUN pip install pdm
# # disable update check
ENV PDM_CHECK_UPDATE=false
COPY pyproject.toml pdm.lock README.md /project/

WORKDIR /project
RUN pdm install --check --prod --no-editable

ENV PATH="/project/.venv/bin:$PATH"

WORKDIR /project
COPY src /project/src

# ENTRYPOINT [ "python", "app.py" ]
